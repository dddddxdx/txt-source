有些髒的牢中，身上只穿著內衣被投入牢中的兩個美女和兩個男人。
那是Ａ級同時也持有稱號的冒険者，冰帝克麗絲塔・尼潔・布蘭里赫特的隊伍。

蓋倫是擔任隊伍前衛的戰士，是孤兒院出身的冒険者。他有著劍的才能。僅僅以此為武器作為冒険者過生活、接濟孤兒院，那樣的人品為人所知後被冰帝，正確來說是被作為她的親友的學友莎夏勸誘加入了克麗絲塔的隊伍。

莎夏是在王都的學院上學的克麗絲塔的學友。單方面迷戀上了儘管是貴族卻對平民的艾夏也平等接觸的克麗絲塔是她加入隊伍的理由。她是侍奉愛之神愛洛斯的神官，擔任了隊伍的回復役。

然後最後是犬的獸人，阿爾。他是靠作為獸人的特徵的敏銳嗅覺和優秀五感擔任隊伍斥候的老練冒険者。是在王都接受委託的時候變親近了，自然的加入了隊伍的伙伴。雖然是經驗豐富的最年長的人，但是因為獸人壽命比人更長所以儘管阿爾年紀超過了四十但外表看起來就像是二十都沒到的少年一樣。

她們是在王都也屈指可數的冒険者隊伍，將據點移到自由交易都市後也取得了如同實力的功績。
所以，這次的指名委託也只是能輕易達成的難易度，誰都沒有擔心過。

無論怎麼說，持有異名的冒険者在王國有沒有十人呢。
如果杜蘭不在的話即使是除掉克麗絲塔的三人也是能殲滅盜賊的吧。何況，如果克麗絲塔以全力使用魔法的話連同洞窟冰凍保存也很容易。
但是，一切都是如果不在的話，實際上現在她們正被拘束著。

雖說是Ｂ級冒険者，但將魔鋼加工了的腳鐐、手銬不是竭盡全力就能掙脫開的東西。所以，除掉克麗絲塔的三人只是被用那個拘束著。
但是，克麗絲塔被戴上了有通常兩倍的手銬、腳鐐，而且是在魔法陣上受著拘束。再加上，套著被刻了封印了奴隷的魔法的魔封紋的枷鎖。
關於克麗絲塔不做到那種地步的話都無法拘束她。如果她使用了身體強化的魔法的話，會徒手弄彎魔鋼，憑力量撕碎魔法陣並將牢房毀壊吧。

「吶，蓋倫。竭盡全力破壊掉啦，那個手銬」

莎夏已經不知道是第幾次的對蓋倫說。

「所以說，這玩意相當結實啊。作為盜賊拿著的東西來說太上等了。有半個月左右的話感覺稍微能把插銷弄彎點，但要馬上是辦不到的！啊，肚子餓了⋯⋯」
「上當了吶」

阿爾冷靜地說。

「那個糞貴族，什麼為了市民啊──絶對在背後串通了吧！」

莎夏非常可恨地咒罵指名委托主。
但是，就算是陷阱盜賊程度的話也能沒問題地粉碎，那是隊伍的結論。誰都無法責怪，責怪就是否定了自己，否定了自己的隊伍。
所以那些僅僅只是對自己的埋怨。

「抱歉，我的估計太天真了」

沒有抑揚的冷淡的聲音響起。
也成為了冰帝由來的無表情和冷淡的音色。雖然那只是因為感情被巨大的魔力而且和冰親和的特殊性質牽引而能保持冷靜，但初次見面聽見的話誰都會覺得像冰一樣吧。克麗絲塔有普通的感情。不過，那很少表現出來。

「不，克麗絲塔沒有錯哦！說要選盜賊退治的是我」

莎夏立即說。

「對啊，我也沒有反對！」

蓋倫像叫喊一樣地說。

「已經最大限度的警戒了。可是誰也無法預料吧，那個杜蘭竟然在這種地方吶」

沒錯，一切只不過是不可能有的偶然引起的悲劇。

「可是，真的是怪物啊杜蘭⋯⋯壁役兩秒就陷落了，克麗絲塔也不可能能認真戰鬥，可惡」

蓋倫是戰鬥開始後第二快被打倒的。
接敵後，兩秒。被彈開了劍，連同盾被斬崩了。
即使是一流的領域，Ｂ級的戰士，那也是極限了。

「哈哈，那麼我更加沒派上用場吶。明明是斥候卻直到在眼前出現都沒察覺到杜蘭的動靜⋯⋯那是也確實地修了盜賊系的武技（Arts）」

阿爾仿彿沒轍似的說道。

「怎麼會，我在發動回復魔法前就失去意識了哦？沒用的是我」

三人的話題也由於被捉住的事，淒慘而沉重。
結果能和杜蘭戰鬥的只有克麗絲塔。

「『『哈』』」

牢中宛如靈前守夜。
莎夏等人的命運已經決定了。作為奴隷被賣掉。因為杜蘭在所以即使有救援來也沒有意義。
牢中已經充滿了死心。
只除了一人。

「誰」

發出聲音的是克麗絲塔。
既不是聽到了聲音也不是感覺到了氣味，五感沒有表示出任何反應。
但是，聲音從口中漏出了。
那不過是強者的本能作為第六感反應了。所以比獸人的阿爾更早反應了。

「──唔姆，確實有吶──而且樣子相當勾引人──艾夏想要誰？」

娜哈特嚴肅地說。
那句台詞簡直就像閱覽商品的買東西的顧客一樣。
期待了是援軍之類的全體再次露出警戒感。

「真是的，娜哈特大人。請好好地救助啦，杜蘭先生也說了打算之後解放的。還有，娜哈特大人不可以東看西看」
「為什麼，都是女人啊。而且那個藍頭髮的女的相當漂亮哦？而且，杜蘭也說了隨我便哦？」
「強詞奪理是不行的哦？誒嘿嘿，娜哈特大人有我（一個）就足夠了！」

目光多少有些冷淡的艾夏盯著娜哈特。
對此，不由得被氣勢壓倒了的娜哈特噤聲了。

「是自己人，嗎⋯⋯？」

莎夏不安地說。

「那麼，嘛，玩笑先放一邊吧。被囚禁的人包括在乾淨的牢裡的奴隷，和在髒的牢裡的性奴隷嗎。暫且，先想辦法解決這個惡臭吧，這樣子都沒法說話──空氣創造（Air Create）」

那是作為陷阱之一的真空房間的應對被使用的風魔法。
雖然衝走了污濁的空氣，但馬上又會被污染吧。

「我是娜哈特大人的從者，艾夏。各位是被抓住的冒険者吧。馬上就放你們出來請放心」

那樣說後，艾夏到處找大概在哪裡的牢房和手銬的鑰匙。

「我們是Ａ級冒険者小隊，冰荊棘。去討伐盜賊卻落得了這副狼狽相。雖然很丟臉但如果您能救我們的話深感榮幸」

莎夏作為代表那樣說。

「比起那種事，那個怪物，杜蘭怎麼樣了！」

是對突然出現的娜哈特她們感到了可疑嗎，蓋倫從旁吼道。
說到底，只要杜蘭在這裡，以交易都市的戰力就怎麼想都不可能會來救援。正因為如此，蓋倫也強烈警戒了娜哈特。

「嗯，啊啊，這會兒正低落著吧？嘛，比起那種事──你們很弱嗎？」

娜哈特像天真的孩子在玩一樣，一邊戳戳牢房一邊問。

「哈？沒聽到話嗎，不是說了我們是Ａ級冒険者小隊嗎！」
「喂，蓋倫。不好意思，那個，我想大概是相當強的⋯⋯特別是克麗絲塔格外強⋯⋯」

莎夏勸告蓋倫。
事實是現在在眼前的不可思議的少女娜哈特掌握著一切。
不能讓她不快。

「呋姆，雖然不知道Ａ級小隊是怎樣的程度，但如果敗給了杜蘭什麼的，果然，怎麼說呢，水平很低吶⋯⋯」

那並不是挖苦，僅僅只是真的是那樣想的所以從口中漏出了而已。
成為娜哈特之後，像曾經的相川徹一樣的日本人般的顧慮一點都沒了。

「哈？你在小瞧我們嗎？找死嗎，啊啊！」
「喂，蓋倫！」

儘管嘴上提醒蓋倫但莎夏在內心也很不快。
杜蘭是所謂的例外，莎夏也是Ｂ級冒険者，而且有著作為Ａ級冒険者隊伍的確實的實績。被否定了那個的話，心裡會涌起憤怒是當然的。

「哈哈，讓你不快了的話抱歉。那麼，不好意思你們等下再說，首先是──」

娜哈特沒有看向關了四個人的一邊，而是看向像精神恍惚一樣眼神死了的奴隷那邊。
將破破爛爛的牢房的鐵格柵用爪一閃，反過來再一閃。
僅僅如此，鐵就像黃油一樣斷了。

「『『哈啊！？』』」

無視被囚禁的三人的聲音，娜哈特悠閑地進入了牢中。
在裡面的是十人左右的女人。全都是裸體，年齡也從十歳左右到四十歳左右都有。
全部人就像捨棄了希望的死人一樣。再加上，從到處都是的裂傷也難以認為身體沒事。其中也有抱有小小的靈魂的人。
有幾個人或是微微挪動，或是說得救了什麼的，但也有小孩子和妙齡的少女之類的沒動的。

「那麼，想活的話就出來吧。想輕鬆的話就沒有痛苦地把你送去那個世界。欸都，在那之前，雖然不擅長回復系統──治癒之風（Heal Wind）。這樣就能動了吧」

雖然也有全員份的藥水但是因為取出來太麻煩了，所以娜哈特發動了唯一能使用的風系統的治癒魔法。

「騙人，傷竟然──」「好溫暖」「連屁股的洞也」「膜回不來嗎，可惜」

之類的，也有相當能回復的人。
隨便從倉庫裡取出衣服和內衣交給她們，她們就高興地穿上了。
問題是完全不動的兩個人。孩子和二十左右的女性。

「想死嗎？」
「唔，啊，嗚──」

孩子那邊不是。她小小搖頭了。
僅僅只是在警戒著嗎。就連對美少女的娜哈特的身姿，少女也看起來像是抱有恐懼一樣。
年齡大約才過十歳的少女。
不可思議的是，儘管她拚命動嘴巴吐出了空氣，卻幾乎沒有發出聲音。拚命想要說什麼，儘管如此還是以失敗告終了。
一定是知道光是不知不覺發出話語，正確來說是發出悲鳴就會產生暴力，便自然的失去了吧。

「──乖乖，喏，已經沒事了哦」

慢慢地摸頭後溫柔地抱起。
雖然娜哈特基本是作為上位者行動，但並不是沒有慈愛。
即使一半是龍，另一半也是人。
雖然不會積極地幫助同族，但娜哈特覺得在目所能及的地方的話也可以幫一下。

「唔，嘻，咦，噎，呃」

少女的意思儘管不成話語也是作為思念由靈魂發出的。儘管不成話語還是拚命的向娜哈特傳達感謝。

「啊啊，好好努力了吶。後面就都交給姐姐了」

給抱起的少女穿上手感無比柔軟的白熊長袍後擔在背上。

「接下來，最後一人了。你怎麼辦？」

在牢房的角落，背靠牆壁的一個女人。那身體簡直就像丟了魂一樣。

「⋯⋯⋯⋯」

沒有回答。
儘管傷治好了，心是治不好的。
眼睛中沒有光，完全感覺不到意思。那是認清了什麼但還是死心了的瞳色。
一定，已經──

世上很殘酷。生活方式很辛苦的現實即便在和平的日本也有。何況如果是在這樣的異世界的話要多少就存在多少吧。
死了比較好，那邊比較輕鬆，如果這是她所希望的──至少作為掌管靈魂的龍安詳地送她上路就是娜哈特的善行。

「已經，可以了嗎？」
「⋯⋯⋯⋯」

女性慢慢閉上了眼睛。
那就是回答吧。

「是嗎⋯⋯那麼，至少讓我給你安樂的死亡──永遠的睡眠（Fallen Hypno）？」

魔法的發動被打斷了。
做了那個的是在背後的小少女。
她以孱弱的力氣拉了背後。

「唔，啊。唔，嗯」
「不行，嗎？但是，已經──」
「啊，呦，呃，哦──呃，咦，嗚，噢，唔，呦，啊，呃」

（她救了我，這次輪到我救她了，嗎）

在二人之間大概有著儘管辛酸但互相幫助的回憶吧。
娜哈特打動不了的女性的心，如果是這個孩子的話也許能讓它動。
為那樣的一縷希望，娜哈特稍微提供了幫助。

「是嗎，那麼加油──魂魔法（Soul Magic）──魂同一化（Soul Engage）」

一個魔法只是橋樑。
不過是小小的希望之線。
失去言語的少女和不再說話的女性，娜哈特的魔法將二人維繫。