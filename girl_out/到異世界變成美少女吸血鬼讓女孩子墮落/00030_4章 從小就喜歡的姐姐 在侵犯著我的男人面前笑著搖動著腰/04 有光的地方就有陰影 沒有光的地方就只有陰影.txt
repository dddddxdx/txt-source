放出的光球撕裂了艾莉絲的爪子。
無法保持球形的魔力塊，破裂了開來，歪曲著形狀發生了爆炸，放出衝擊波後就消失了。

「咕！」

用手臂遮住掩埋一切視線的閃光，用腳忍耐住快要將自己吹飛的爆炸衝擊波。
也許是因為被光線照射的緣故，皮膚有一種被灼熱的感覺，但還不至於疼痛的叫出來的程度。
在衝擊波平息到某種程度後，不知不覺下一個光球已從前方逼近，向旁邊跳躍回避它，然後快速鎖定蕾雅。

放出魔法的果然是那個人類，人偶的莉娜只是個用來溝通的道具而已。
為什麼不用本人的嘴說話 ── 艾莉絲雖然有著這樣的疑問，但是很快就放棄了思考。
因為，「反正都會死，所以沒有必要去想」

「啊啊啊啊！」

艾莉絲向著蕾雅跳躍。
打算用爪子一口氣割掉腦袋。
蕾雅雖然在視野裡捕捉到了她的身姿，但她連回避運動都沒有採取。
恐懼使身體無法動彈嗎 ── 還是說還有其他防御手段呢？
艾莉絲驚訝的思考著，即使被某種魔法所攔截，也要有相應的對策。

『真是單調啊，果然人外什麼的就這種程度吧』

聽到了人偶的聲音。
然後莉娜，舉起了布做成的拳頭，擋在蕾雅面前。

「布娃娃是不可能擋的住我的！」
『我要守護蕾雅，不然，莉娜就沒有意義』

莉娜跳了起來，接住了朝蕾雅揮下的爪子。

哐，就像是撞擊到金屬上一樣的觸感。

不單單只是普通的布，恐怕是用魔法強化過了吧。
艾莉絲暫且和人偶保持著距離。
但是莉娜不會讓她逃走，黑貓人偶用兩隻腳靠近著慌張的艾莉絲，跳躍起來，並且連續向著臉，腹部，還有腳放出拳頭。

『放棄吧，即使躲開了，你也沒有勝機！』

艾莉絲繼續回避著莉娜的攻擊。
但她也知道這樣繼續下去是贏不了的。
因為敵人不僅僅只有莉娜，蕾雅已經產生出了新的光球，準備朝著這邊發射。

「真礙眼啊！」

艾莉絲瞄準莉娜著地的時機，反覆踢了過去。
但是人偶並沒有進行回避。
從正面接住了艾莉絲的攻擊，雙手緊緊抓住艾莉絲的腳。

「騙人！呀啊啊啊！？」

然後就那樣把艾莉絲拖倒在地上，然後繼續旋轉揮動著被抓住的愛麗絲。

『所以我才說了，要有自知之明！』

莉娜放開了手。
而後艾莉絲的身體被甩了出去，就那樣撞到了牆壁。
響徹了整個城堡的破碎聲。
她陷入了用磚塊砌成的牆壁中，慢慢地一點一點向下滑落，筋疲力盡地倒在了地上。
蕾雅把比剛才更加巨大的光球轉向那裡。

『雖然不能理解明明是吸血鬼，卻不是很怕光，但這樣就結束了。那麼，吸血鬼，至少要在那個世界裡好好反省一下為什麼要對人類出手』

莉娜扔下這句台詞後，光球就離開了蕾雅的手。
終於恢復了意識的艾莉絲，抬起頭看著它靠近，但是身體卻動不了。

── 姐姐大人，會生氣吧。

擅自出走，擅自想要殺掉 都，然後擅自死去。
想必千草也會厭煩了吧，僅僅因為嫉妒而暴走還喪命的眷屬，拋棄也是理所當然的。

「對不起，姐姐大人⋯⋯我，真是笨蛋⋯⋯」
「是啊，真是太亂來了」
「誒？」

在艾莉絲的耳旁，聽到了死前最想聽的，即便是死掉了也想聽的最喜歡之人的聲音。
本以為是臨死前的幻聽，卻從艾莉絲的影子裡出現了她的身姿。

「但，這都是因為我的模棱兩可而引起的。論笨蛋的程度，真是彼此彼此啊，艾莉絲」

於是千草擋在了艾莉絲的面前，用伸出的左手接住了光球。
但是，光球卻變換了姿態纏繞在了手臂上。
和剛才撕裂了艾莉絲爪子的魔法完全不一樣。
將包入的手臂燒盡，消滅之 ── 這就是蕾雅放出的魔法。

「姐，姐姐大人，手臂！？」
「原本以為可以接下來，沒想到是這麼強的魔法」

千草冷靜地說道。

『新的吸血鬼⋯⋯而且從使用了影子這一點來看，你應該是頭目吧。但是，在失去了一隻手的狀態下是無法戰勝我們的！』
「失去了一隻手？」

千草歪著頭，往左腕的切斷面上稍微注入了力量。
然後 ── 從那裡溢出滑溜溜的影子，形成應該已經失去了的手臂。
不久後，影子變化成了原本的膚色，原本失去的左臂就完全再生了。

『不可能，就算是吸血鬼，也不可能有這樣的再生速度！』
「不知道其他吸血鬼的情況所以就不多加說明了，不過，這種程度的話，只要使用了影子就是輕而易舉的事情」

千草的態度裡滲出了一絲從容。
蕾雅面對著那樣的她，感受到了與艾莉絲不一樣的等級上的差異。
沒有感受到像殺意那樣的壓力，那種不自然反而煽動著恐懼情緒。

『也就是說，魅惑了 都 的人是你嗎？但是很遺憾，魅惑的詛咒已經解除了，她再也不會去見你了』
「⋯⋯這樣啊」

千草臉上的表情很複雜，只有在一瞬間垂下了雙眼。
不知道是該悲傷還是該高興。

「話說回來你，認識御影老師的吧」
『這種稱呼方式⋯⋯和轉移過來的學生們一樣，難道你也是轉移者嗎？不會吧，難道是 ── 轉移的同時，死掉了的日向千草？』
「哦呀，你也知道我嗎」
『為什麼會以那種狀態活下去⋯⋯不，成為吸血鬼這件事，也就是說你的身體裡肯定存在著吸血鬼。不是只有那傢伙嗎 ──』

在蕾雅的腦海中浮出來的是，之前為了魅惑「公主」而潛入城中的一名吸血鬼。
聽說最後被注意到公主異常狀態的騎士斬殺，退治，將屍體扔到了垃圾場。

『這群騎士們，居然還說已經處理掉了吸血鬼卡米拉，是誤以為殺了嗎？而且，還偏偏把力量轉讓給了轉移者 ── 不要開玩笑了，也就是吸血鬼力量和轉移者力量的合成物，這太危險了！』
「雖然我不知道你在說什麼，但是我確實被卡米拉救了下來。不，正確來說應該是互相救下了對方的生命」
『不行，不行，不行，不能放任你這樣的怪物。必須要在這裡處理掉！』
「⋯⋯感覺說話不通，是錯覺嗎？」

千草有點寂寞地問了艾莉絲。
就在這對話的間隙，莉娜一瞬間逼近了千草的背後。

「姐姐大人，背後！」
「我只是想帶艾莉絲回去」

千草回過頭來，一隻手接住了莉娜的拳頭。
非常輕易。
像是普通人抓住了普通的布娃娃一樣。

『力量和那邊的吸血鬼完全不一樣』
「太過小看艾莉絲的話，我可是會生氣的哦？」
『切！』

莉娜和千草保持著距離。

『雖然收拾起來很麻煩而不太想用⋯⋯！咕，啊啊啊啊啊啊啊啊啊！』

莉娜的身體 ── 布娃娃的布皮刺破，從內側出現了無數的刀刃。
大概是計劃利用敏捷的動作，用那些刀刃斬擊吧。
蕾雅低聲咏唱著，周圍浮現出了無數的光球。

但是即使看到了這些，千草的從容也沒有動搖。
和往常一樣浮現出了溫柔的微笑。

『不要以為那張裝模作樣的臉，能繼續下去！』

莉娜踢了下地面，向千草發動特攻。
千草慢慢地向前伸出了右手。
於是，在她背後的影子裡，出現了無數的黑手，纏住了莉娜。

『什⋯⋯影子⋯⋯！』

然後就這樣用影子溫柔地把全身包入。

咔嚓。

一口氣從多個方向扯裂了人偶。

「斯！？」

蕾雅的表情被恐懼所扭曲。
注入了大量的魔力，本應被強化了的人偶，居然如此輕易地就被破壊了。

確信了。

在自己面前出現的這個，叫做日向千草的吸血鬼 ── 比至今為止戰鬥過的任何敵人，要遠遠強大得多。
所向無敵，王国引以為豪的最強魔女，那就是蕾雅。
與人類為敵的話，即使是擁有強大魔力的轉移者也無法對抗，但即使是那樣的她。

「對不起」

睜開眼睛看著千草的蕾雅，看到了她深深低下了頭，覺得非常驚訝。

「我以為只是個人偶而已，但從反應來看，應該是很重要的人偶吧」
「⋯⋯嘶⋯⋯」
「可以恢復到原狀，所以請不要那麼害怕」

影子再次從千草的背後伸長，收集著零散的莉娜的零件。
然後影子填補了零件之間的縫隙，一轉眼間黑貓人偶又恢復到了原來的形狀。

「還有，送給你作為道歉的禮物」

但是，被破壊過一次的人偶，已經沒有了蕾雅的魔力。
應該只是個布娃娃。
可是莉娜自己站了起來，靠近著蕾雅。

『呀，蕾雅，我是莉娜哦』
「嘶⋯⋯！？」
『多虧了蕾雅的愛，我才擁有了自我』

莉娜慢慢地靠近著蕾雅。
蕾雅看到那個了身姿後，拚命地左右搖頭。

不對，不對，這可不是莉娜，這不是。

「根據布娃娃上留下的記錄形成的人格。當然沒加入任何其他的東西，我想比起以前一定能相處的更好」

千草只有善意。
真的，從心底，認為這是好事，所以再生了人偶，給予它自我。

但是蕾雅 ── 正因為能理解這是善意，所以才莫名其妙，無法接受。

手掌裡浮現出光球。
她對著莉娜放出了那個 ──

『蕾雅，為什麼要做那種事？等一下，蕾雅，我一直守護著蕾雅。因為，我喜歡蕾雅⋯⋯啊啊啊啊啊啊啊啊！好熱啊，好熱啊，救救我，蕾雅，蕾雅啊⋯⋯啊！』

莉娜這樣說著，千草慢慢地，接近著茫然失去自我的蕾雅。
倒在地上的艾莉絲露出了沉迷的表情看著那個樣子的千草。
這就是，這就是自己深愛的姐姐大人。

「⋯⋯別，別過來⋯⋯」
「終於聽到你的聲音了。請放心，今天我沒打算魅惑你」
「⋯⋯不，不要⋯⋯」
「但是道歉，必須要好好的做。我想這一次，一定會讓蕾雅小姐高興」
「不⋯⋯不⋯⋯」

恐懼到歪曲了表情的蕾雅仰視著千草，背後的影子慢慢地向她的身體伸長。
沒有明確的形狀，像泥一樣的黑暗。
像是要完全覆蓋蕾雅的身體一樣爬著。

「ā⋯⋯áa⋯⋯」

最初發出了恐懼聲音的蕾雅 ──

「én，ā⋯⋯à，Há⋯⋯ Há áa⋯⋯」

接下來的聲音漸漸地充滿了熱度。
不知不覺間蕾雅穿著的哥特風格的衣服被溶化了，全身完全，被影子所包入。
那些全部，都可以說是千草的手掌。

「沒有直接觸摸的話，應該就不會被魅惑。現在開始慢慢地，愛你，讓你舒服」
「Há，Há Yá ū⋯⋯Yà，úu⋯⋯！」

蕾雅的身體顫動著，痙攣著。
襲擊著全身的是，人類所無論如何也不能給予的，人外的喜悅。
把那個，給予了幾乎沒有那方面知識的蕾雅。

無法抗拒。

「én á，á áa，Yá，Yá Ráa，不要⋯！」
「明明是這麼可愛的聲音，卻一言不發真是太可惜了」

蕾雅的臉紅著，眼睛濕潤著。
她的大腦已經完全被這種感覺所支配，連對千草的恐懼都消失了。
千草確認蕾雅開始沉溺了，更加擴大了影子。
從頭到腳的影子慢慢向上爬，最初是侵入了嘴裡。

「Há，Há Gá ⋯⋯ò，ó óo ó ó⋯⋯！」

從舌頭到喉嚨，從喉嚨到食道，肺，甚至是腸胃 ── 不僅是外側，影子也進入到內側。
不久後，不僅僅是嘴巴，耳朵，鼻子，眼睛，進入到了所有的間隙，蕾雅品味著內心都快要被破壊的快樂。

「ó，ó Hí，Hí，Yá⋯⋯èn，Gáa，á áaaaa á！」

從聲音上可以辨別出是蕾雅，但外表上已經是完全黑色的一塊。
千草非常高興地望著就像是快要死去的幼虫般顫抖著身體的她。

影子從蕾雅大腦裡的每一個細胞開始滲透，支配，直到全部，甚至除去了逃跑的意志。
總之，除了感覺到「好舒服」以外的所有一切，都被奪走了，直到少女意識模糊為止，她的肉體仍保持著純潔的狀態被持續玷污著。


◇◇◇

蕾雅的反應逐漸變得淡薄，千草除去影子解放了她。
被影子吞沒的衣服也復原了，還有根據之前的反省重新再生了黑貓人偶的莉娜。

「等，等⋯⋯一下⋯⋯」

顫抖著身體，流下眼淚和口水的蕾雅，對著離去的千草，拚命的伸出了手。
當然這是無法傳達的，但千草被她的強大精神所感動，停下了腳步。

「姐姐大人？」
「請稍等一下，艾莉絲。我想給讓她幫我傳個話」

這樣說著，走近了蕾雅，蹲下來把嘴湊近到她耳旁。

「請給御影老師傳個話」
「我⋯⋯，為⋯⋯什麼⋯⋯」
「只是道別而已」

儘管蕾雅沒有理解，不過，千草卻單方面說道。

「『對不起，至今為止多謝了，都姐姐』，請傳話給她」
「都⋯⋯姐？」

雖然是很簡短的一句話，但是如果能傳達到的話就足夠了。
如果今天是最後一天，和 都 再也不會相見的話，那就可以了。

千草不知為什麼這麼想著。

說到底，這就是艾莉絲不滿的原因，千草身體中所殘留的「人性的部分」

但是，如果不再和 都 相見，那也就到此為止吧。
千草已經不會再懷著人類的感情了，只作為吸血鬼，為了實現自己所期望的世界而繼續前進。

「那麼，我們回去吧，艾莉絲」
「嗯⋯⋯不生氣嗎，姐姐大人」
「我也有需要反省的地方。但是，即便如此還是希望我生氣的話，那我就考慮懲罰你一下」

千草這樣說著，妖媚地笑了。
看著那個表情，艾莉絲染紅了臉。
因為馬上就能想像出要被做些什麼。

「⋯⋯那麼，那麼，就請懲罰我吧」

抱著害羞的說著話的艾莉絲的身體，兩人溶化在了影子中，最後消失了。
蕾雅看著那個景象，什麼也做不了。只能目送著。

被自己的無力所打垮 ── 在兩人消失的瞬間，緊繃著的線斷了，她失去了意識。