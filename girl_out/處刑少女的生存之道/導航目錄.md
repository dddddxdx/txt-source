# CONTENTS

處刑少女的生存之道  
処刑少女の生きる道  
处刑少女的生存之道  

作者： 佐藤真登  



- :closed_book: [README.md](README.md) - 簡介與其他資料
- [含有原文的章節](ja.md) - 可能為未翻譯或者吞樓，等待圖轉文之類
- [待修正屏蔽字](%E5%BE%85%E4%BF%AE%E6%AD%A3%E5%B1%8F%E8%94%BD%E5%AD%97.md) - 需要有人協助將 `**` 內的字補上
- [格式與譯名整合樣式](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E8%99%95%E5%88%91%E5%B0%91%E5%A5%B3%E7%9A%84%E7%94%9F%E5%AD%98%E4%B9%8B%E9%81%93.ts) - 如果連結錯誤 請點[這裡](https://github.com/bluelovers/node-novel/blob/master/lib/locales/)
-  :heart: [EPUB](https://gitlab.com/demonovel/epub-txt/blob/master/girl/%E8%99%95%E5%88%91%E5%B0%91%E5%A5%B3%E7%9A%84%E7%94%9F%E5%AD%98%E4%B9%8B%E9%81%93.epub) :heart:  ／ [TXT](https://gitlab.com/demonovel/epub-txt/blob/master/girl/out/%E8%99%95%E5%88%91%E5%B0%91%E5%A5%B3%E7%9A%84%E7%94%9F%E5%AD%98%E4%B9%8B%E9%81%93.out.txt) - 如果連結錯誤 請點[這裡](https://gitlab.com/demonovel/epub-txt/blob/master/girl/)
- :mega: [https://discord.gg/MnXkpmX](https://discord.gg/MnXkpmX) - 報錯交流群，如果已經加入請點[這裡](https://discordapp.com/channels/467794087769014273/467794088285175809) 或 [Discord](https://discordapp.com/channels/@me)


![導航目錄](https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=https://gitlab.com/novel-group/txt-source/blob/master/girl_out/處刑少女的生存之道/導航目錄.md "導航目錄")




## [第一卷 於是，她將蘇醒](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7%20%E6%96%BC%E6%98%AF%EF%BC%8C%E5%A5%B9%E5%B0%87%E8%98%87%E9%86%92)

- [序章](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7%20%E6%96%BC%E6%98%AF%EF%BC%8C%E5%A5%B9%E5%B0%87%E8%98%87%E9%86%92/00020_%E5%BA%8F%E7%AB%A0.txt)
- [第一章 处刑人](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7%20%E6%96%BC%E6%98%AF%EF%BC%8C%E5%A5%B9%E5%B0%87%E8%98%87%E9%86%92/00030_%E7%AC%AC%E4%B8%80%E7%AB%A0%20%E5%A4%84%E5%88%91%E4%BA%BA.txt)
- [Interlude 幕間](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7%20%E6%96%BC%E6%98%AF%EF%BC%8C%E5%A5%B9%E5%B0%87%E8%98%87%E9%86%92/00040_Interlude%20%E5%B9%95%E9%96%93.txt)
- [第二章 啟程](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7%20%E6%96%BC%E6%98%AF%EF%BC%8C%E5%A5%B9%E5%B0%87%E8%98%87%E9%86%92/00050_%E7%AC%AC%E4%BA%8C%E7%AB%A0%20%E5%95%9F%E7%A8%8B.txt)
- [Interlude 幕間](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7%20%E6%96%BC%E6%98%AF%EF%BC%8C%E5%A5%B9%E5%B0%87%E8%98%87%E9%86%92/00060_Interlude%20%E5%B9%95%E9%96%93.txt)
- [第三章 王都郵寄來的恐怖份子](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7%20%E6%96%BC%E6%98%AF%EF%BC%8C%E5%A5%B9%E5%B0%87%E8%98%87%E9%86%92/00070_%E7%AC%AC%E4%B8%89%E7%AB%A0%20%E7%8E%8B%E9%83%BD%E9%83%B5%E5%AF%84%E4%BE%86%E7%9A%84%E6%81%90%E6%80%96%E4%BB%BD%E5%AD%90.txt)
- [Interlude 幕間](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7%20%E6%96%BC%E6%98%AF%EF%BC%8C%E5%A5%B9%E5%B0%87%E8%98%87%E9%86%92/00080_Interlude%20%E5%B9%95%E9%96%93.txt)
- [第四章 古都](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7%20%E6%96%BC%E6%98%AF%EF%BC%8C%E5%A5%B9%E5%B0%87%E8%98%87%E9%86%92/00090_%E7%AC%AC%E5%9B%9B%E7%AB%A0%20%E5%8F%A4%E9%83%BD.txt)
- [Epilogue 尾聲](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7%20%E6%96%BC%E6%98%AF%EF%BC%8C%E5%A5%B9%E5%B0%87%E8%98%87%E9%86%92/00100_Epilogue%20%E5%B0%BE%E8%81%B2.txt)
- [後記](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7%20%E6%96%BC%E6%98%AF%EF%BC%8C%E5%A5%B9%E5%B0%87%E8%98%87%E9%86%92/00110_%E5%BE%8C%E8%A8%98.txt)

