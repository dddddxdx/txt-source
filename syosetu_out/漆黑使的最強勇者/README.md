# novel

- title: 漆黒使いの最強勇者　〜仲間全員に裏切られたので最強の魔物と組みます〜
- title_old: 仲間全員に裏切られた勇者最強、引退して魔物最強とパーティを組む
- title_zh: 漆黑使的最強勇者 被所有夥伴拋棄後與最強魔物為伍
- author: 瀬戸メグル
- illust:
- source: https://ncode.syosetu.com/n2477eq/
- cover: https://images-na.ssl-images-amazon.com/images/I/51K2EAEJ35L.jpg
- publisher: syosetu
- date: 2019-12-06T20:35:00+08:00
- status: 連載
- novel_status: 0x0000

## illusts

- ジョンディー
- 木村 有里

## publishers

- syosetu

## series

- name: 漆黒使いの最強勇者　〜仲間全員に裏切られたので最強の魔物と組みます〜

## preface


```
『暗之勇者』西恩遭到了小隊成員們的無情背叛。
就在他放棄求生的希望，徘徊在森林中尋死時，一隻魔物出現在了他的面前。
「可以殺掉我嗎？」
魔物拜託失去了力量卻依然強大的西恩殺掉自己。
但自身也一心求死的西恩所選擇的卻是——

※旧題『仲間全員に裏切られた勇者最強、引退して魔物最強とパーティを組む』

　歴代最強と名高い【闇の勇者】シオン。彼には信じるものが一つあり、それは今のパーティメンバーだった。
「この最高のパーティで魔王を倒す！」

　そう固く誓ったシオンだが、その矢先に悲劇が起きる。なんと信じていた彼女達から酷い裏切りにあってしまったのだ。
　辛うじて一命をとりとめるも、シオンは心に深刻なダメージを受ける。
　生きることを諦め、死のうと森を彷徨う彼の前に一体の魔物が現れた。

「私を殺してくださいませんか？」

　力を失っても強いシオンに、魔物は自分を殺してくれと頼む。
　しかし自分もまた死にたかったシオンの取った行動は――

※短編「俺が信じていた仲間は全員、悪徳勇者に送り込まれた刺客だった」の連載版です
```

## tags

- node-novel
- esjzone
- node-novel
- R15
- syosetu
- スローライフ
- ハイファンタジー
- ハイファンタジー〔ファンタジー〕
- ファンタジー
- 主人公最強
- 勇者対決
- 復讐はしない
- 旅
- 残酷な描写あり
- 相棒は白狐
- 裏切り
- 闇魔法を極めた青年

# contribute

- 人妻妹抖御姐峰
- 涼宮千葉

# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 1
- startIndex: 0

## esjzone

- novel_id: 1572969953

## syosetu

- txtdownload_id:
- series_id:
- novel_id: n2477eq

## textlayout

- allow_lf2: true

# link

- [narou.nar.jp](https://narou.nar.jp/search.php?text=n2477eq&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [小説情報](https://ncode.syosetu.com/novelview/infotop/ncode/n2477eq/)
- http://www.dm5.com/manhua-qiheishidezuiqiangyongzhe-beisuoyouhuobanpaoqihouyuzuiqiangmowuweiwu/
- [漆黑使的最强勇者吧](https://tieba.baidu.com/f?kw=%E6%BC%86%E9%BB%91%E4%BD%BF%E7%9A%84%E6%9C%80%E5%BC%BA%E5%8B%87%E8%80%85&ie=utf-8 "漆黑使的最强勇者")
-

