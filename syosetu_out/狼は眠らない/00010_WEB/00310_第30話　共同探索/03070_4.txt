一月二十五日。

這天早上，雷肯原本打算再去一次斡旋所，但放棄了。
說到底，不中意這裡的設施。
石造的誇大接待和嚮導所。
規模過大的收購所和販賣所。
最重要的是，林立的住宿設施。

甚至從非常便宜的宿屋到難以置信的高級宿屋都備齊了。
這本身雖然並非壊事，但雷肯看了一眼，就抑制不住湧出的厭惡感。

這是奴隷的待遇。
依據工作量，飯的品質和量會變好，床也會變成上等貨，但在被管理和被要求去工作這點上，終究跟奴隷相同。

也不怎麼清楚是什麼讓自己有這種感受。
平淡地，流利地應對冒険者們的職員，看起來像家畜小屋的看守，這說不定是原因。

當然，冒険者們是以自己的意志待在這裡，以自己的意志探索迷宮。儘管如此，看到機械性地一手處理那冒険的進行方式和取得品的職員們，不知為何心情就會變得難受。
雷肯本來就討厭石造的建築。不覺得在那種地方裡能安心。所以就算隔了距離，有些昂貴，也選了〈拉芬的岩棚亭〉，到今天也不後悔那選擇。

比起這個，斡旋所。
在這迷宮，隊伍之間似乎很難毫無計畫來共同探索的樣子。想共同探索的隊伍會到斡旋所申請。沒有帶上共同探索的對象到迷宮去的，就是沒打算共同探索的隊伍。

果然，要前往更深的階層，不斡旋隊伍就辦不到。
但是，實在提不起勁。
而且，現在馬上過去，就會被那個職員盯上吧。

總而言之，決定跟阿利歐斯兩人再前進幾階層看看。
兩人雖然以〈轉移〉轉移到了八十六階層，但放棄馬上突入頭目房，進了普通個體的房間。
出現了五隻〈黑肌〉和四隻〈赤肌〉。
八十階層以來都是這個比例。

以跟昨天相同的，在最初擊出〈炎槍〉的戰法來戰鬥。擊出的〈炎槍〉比起貫穿力更注重給予衝擊力重量。比起槍，更像攻城槌的攻擊。
之後，雷肯首先扼殺〈黑肌〉的機動力，阿利歐斯則以〈赤肌〉為重點來打倒，戰鬥沒多久便結束了。

不知為何，最初對〈黑肌〉擊出〈炎槍〉後，四隻〈赤肌〉就都會把雷肯認定為攻擊對象的樣子，所以阿利歐斯能無傷移動到方便的位置上。阿利歐斯開始攻擊的話，附近的白幽鬼也會注意起阿利歐斯，所以能順利分散敵人來擊倒。

有一隻〈黑肌〉拿了奇妙的武器。裝在手掌上，握住手來使用的四支爪的鉤爪，挺有重量的，也有長度。然後是恩寵武器。阿利歐斯沒有說那是恩寵武器。阿利歐斯的眼力對劍以外的武器不太適用吧。

施展〈鑑定〉的雷肯不自覺地，阿，了一聲。

「怎麼了嗎？」
「這武器是，〈搔山爪〉。攻擊力高得異常阿。恩寵是〈威力附加〉，但那附加量很可怕。我想有十倍以上」
「誒誒誒」
「不只如此。附有〈武器破壊〉這恩寵，接觸了這鉤爪的武器有三分之一的機率會被破壊的樣子」
「啊啊。這一類恩寵，會根據上下關係，現出或不現出效果喔」
「上下關係？」
「對作為恩寵品的格較高的對象通用不了」
「作為恩寵品的格較高？」
「是的。但這裡畢竟是迷宮的八十階層帶呢。一般的恩寵品沒辦法對抗吧」
「作為恩寵品的格，要怎麼比較？」
「誰知道呢。〈鑑定〉應該能得知就是了」

雷肯取出在五十階層入手的〈吸命劍〉，意識著要比較作為恩寵品的格來施展〈鑑定〉。也對鉤爪做了同樣的事。

「喔。鉤爪的格比較高阿」
「能知道嗎？」
「也有這種鑑定項目阿。上了一課」
「得到好武器了呢」
「但是我沒有要用。賣掉吧」
「誒誒？」
「你要用嗎？」
「不用」
「是吧？阿，對了」

雷肯對〈拉斯庫之劍〉和鉤爪，以比較哪邊的格比較高的印象，鑑定了看看。

「如何呢？」
「不怎麼清楚差異」
「這樣啊。但是，對象不是恩寵品的話，我覺得〈武器破壊〉的恩寵恐怕會直接算有效喔」
「是那樣阿」

三分之一的機率會破壊，代表必須當作交鋒了三次就會被破壊掉。

〈拉斯庫之劍〉是把好劍。是蘊藏了打造者的魂魄的劍。這會被迷宮出的恩寵品輕易地打碎，感覺實在不講理。

之後，攻略了兩間普通個體的房間。雖然沒出恩寵品，但出了高品質的長劍。
在空房間吃了飯。吃飯途中雖然有冒険者進來了，但看到有雷肯和阿利歐斯在，就默默地出去了。