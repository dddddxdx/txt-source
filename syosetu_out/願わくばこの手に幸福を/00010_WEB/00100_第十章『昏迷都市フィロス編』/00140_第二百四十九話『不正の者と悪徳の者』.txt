來了，來了，來了。惡魔發出腳步聲走過來了。

羅佐背著肩膀，從菲洛絲的城門俯視著那個景象。眼皮不知不覺地搖晃著。心臟發出高亢的叫聲。

紋章教的一隊部隊就像被狗追趕的羊一樣，在街道上奔跑，向這邊走來。他目不轉睛，沒有在意其他的事。
當然了。背後被石子彈襲擊，還能意氣風發地向前衝的傢伙是不存在的。而且那些傢伙雖然有武裝，但本來就沒有戰鬥的意識。這樣的話，身心都準備好戰鬥是不可能的。

所以，會來的。

誤以為是山賊什麼的襲擊，然後到達菲洛絲的城門前。羅佐在腦海中反覆嘟噥著，就好像告訴自己一切都會順利進行。

今天羅佐沒有虛張聲勢的餘力。所謂戰鬥和戰術，對他來說是太過超出能力的事。對於只能斷言自己的舌頭的能力的他來說，沒有交談的戰爭肯定是應該回避的。

果然，應該取消行動嗎？像這樣從自己的手中放棄的話，如果現在這樣的話，也許就能挽回了。如果自己一個人去死，也許一切都會結束。

作為紋章教軍，應該還不知道菲洛絲兵已經直接露出了獠牙。如果是現在的話，也許還可以回到原來的樣子。
但是，如果正式將紋章教軍轉為敵人，就完蛋了。正如菲洛絲・托雷特所說的那樣，菲洛斯這個城市會滅亡吧。這件事，羅佐自己也非常理解。

因此，如果現在折回去的話。還能夠將一切平穩地收拾好。
羅佐閉上眼睛。然後一邊清理喉嚨，一邊煽動周圍的部下，還有在門前配備的都市士兵們的耳朵和心臟。

「──自治民的諸位！敵人來了。惡魔肯定會來到這裡。這是神的意志決定的。」

高亢，堂堂正正。羅佐的聲音撫摸著市民們的耳朵。對他來說，這是再沒有比這更擅長的事了。
羅佐的表情，破裂了。

「卡拉伊斯特王国和大教堂和我們約定了！為了討伐邪教，給了我們很大的支援！膽怯著的各位，現在正是在這裡忍耐，才能得到拯救！」

像要吸入他人腦海，傳到到臟腑深處一樣，羅佐說。

紋章教的惡魔們想要吞噬這座城市。不戰則亡。在這裡忍耐，就會得到援助。這種話好幾次都灌輸到給市民們聽的話語中。
雖然看上去很傻，但對於身為大聖教徒的市民來說，羅佐所說的話卻是真實的。

紋章教徒是惡魔和畜生之類的，任何時候都在瞄準自己。每位市民小時候都被這樣述說，他們總是在尋找機會吃掉我們的腦袋。他們不是同一種人。
同樣，如果真的遭受痛苦的時候，神會給予我們救助。

正因為如此，對於市民來說不可能握起惡魔之手。與惡魔接吻的菲洛絲・托雷特是出賣靈魂的背德者。一心相信善良的市民們不會懷疑這一點，他們認為一旦靠近背德者和紋章教的話就會被奪走靈魂。

所以他們相信，羅佐一定不會騙他們的吧。羅佐只是，在市民們耳邊低聲私語著他們所謂的理想，給予他們自認為的未來，灌入了他們相信是正確著的手段。僅僅如此而已。

譬如，自己從卡拉伊斯特王国那裡收到的密書，這樣的事絶不會跟這些市民透露。
有時真相是再怎麼擦拭眼睛也看不到的。


◇◆◇◆

自治都市菲洛絲的周圍，是變得稍微開闊的平原。因此，從城市到遠處都能很好地看清，只要出現山賊，城市兵馬上就能趕到。因為沒有險峻的山脈，所以街道的交通也極為方便。

大概是多虧了在這種適合交易的地方建立了都市的緣故吧，聽說以前菲洛絲這個城市，作為直通卡拉伊斯特王国的城市而繁榮起來。

只是作為缺點，所謂的平原很容易配置大量的士兵。也許是因為這個原因，菲洛絲在歷史上應該經歷過好幾次陷落的痛苦。
但是，只有今天這個時候，那個缺點好像取代了優點。

菲洛絲城門前，看著一邊把我和部隊的士兵們全部包圍，一邊手持槍和投石器的菲洛絲的都市兵，我反射性地咬了牙。

「聽說是物資的補充，但看這個樣也兼備了士兵的補充呢，安。」

像嘆氣一樣說。相對於這邊不足一百的小隊，僅僅從正面看都市兵就有七百人吧，而且還突襲了部隊。那麼，當然背後也有士兵在埋伏。也就是說完全被設計了。

看到石子彈的那個時候就開始有不好的預感。腦髓深處有麻痺一樣的觸覺。但是，菲洛絲都市的士兵現在才與紋章教敵對，怎麼會發生這樣愚蠢的事呢。

那樣隨意推測事物的懲罰是中了的吧。結果竟然被逼到這種境地。
雖然不知道是誰寫的這個劇本，但他似乎是相當喜歡從背後陰人的傢伙。惡趣味也要有個限度。

對於我的話，安一邊回答著不可能吧，一邊繼續說。

「路易斯大人。請選十人左右突破回去，剩下的人就在這裡忍耐一下。」

安說，現在馬上。路易斯扭曲了眼，從嘴唇取下咬著的香煙。

畢竟，沒有弄錯那個意思。剛才還在寒冷中顫抖的安的聲音，現在已經相當清晰了。只是聽著就覺得連精神都快要變硬了。
突破圍堵，逃跑。將安和大多數的士兵像蜥蜴的尾巴一樣砍掉，與幾個同伴一起跑回陣地，安已經做好了這個覺悟。

深深地吐出一口氣。你在說什麼？這樣反射性的言詞都快從嘴中露出來了。別說傻話了，那樣的事能辦得到嗎？
但是，回過頭來看安的眼睛，知道了安的話不是很容易說出來的。安在一瞬間做了怎樣的覺悟，做出了怎樣的判斷。

真可怜。我讓比自己小的少女做什麼啊？愚蠢也要有個限度。
吸入了呼吸，再次讓空氣充滿了肺中。這時候。在菲洛絲城門前，響起了聲音。

「──經常光臨的紋章教的各位」

響徹四周，低沉的聲音。城市士兵們為了做出反應，使槍尖搖晃。好歹，他們包圍了我們但沒襲擊過來，好像在等著這個聲音。

有腳後跟發癢的感覺。胸口灼熱。那個啊，他是主謀嗎？難道他是造成這種愚蠢狀況的禍首嗎？

「安，是誰啊，那個從城牆上往下看的傢伙。」

以嘆氣的氣勢，絞盡腦汁想出了一句話。眼睛變細，舌頭有嚴重乾燥的感覺。在聽到安的聲音之前，我的腦海中已經浮現了一個人的名字。

安，勒緊喉嚨發出聲音。

「就是那個，路易斯大人。那就是羅佐。作為菲洛絲民會議場的代表，是我們的合作者的男人。」