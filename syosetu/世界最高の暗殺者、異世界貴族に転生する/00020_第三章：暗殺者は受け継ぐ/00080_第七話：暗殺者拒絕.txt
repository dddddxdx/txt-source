憑借勇者埃波納的技能，根據【跟隨我的騎士們】，我得到了她的力量和一些技能。
立刻，借用了城內的練習場，試著運動了身體，不過，和之前的身體能力有很大的不同。

特別顯著的是魔力放出量的增加。
雖然擁有比常人近千倍的魔力，但一次能釋放的量最多只有十倍左右的弱點，但現在卻有將近三十倍了。

可以說是克服了弱點。
這樣一來，可能的戰術就會一下子增加。

「一點也不像羅格大人。」

氣喘吁吁，塔爾特跪在那裡。
為了測試能力，請他做了對手。
不使用魔力強化身體能力，而與全力的塔爾特戰鬥了。
平常的話，如果做到這個地步，塔爾特已經贏了，但是我今天還是贏了。

「沒想到會變得這麼強。我很期待對塔爾特使用【跟隨我的騎士們】。」
「我也很期待。羅格大人在我心中流淌，一直連接著，非常的棒。」

聽起來有點噁心難道是我的錯覺嗎？

雖然很想馬上給予塔爾特【跟隨我的騎士們】，但是還想進行各種實驗。
想要在某種程度上操縱給予的技能。

在我的技能和從埃波納借來的技能中，特別想交給塔爾特的技能是明確存在的。
在我所有的技能中【突破界限】和【超回復】

這個組合非常凶惡。⋯⋯和魔族戰鬥後明白了。那不是應該以人為對象的。

特別希望塔爾特能超越別人。
並且，在埃波納的技能中【可能性之卵】

能不能挑選這三個交給他們，經過多方考驗，特別給予塔爾特的力量。

「只能讓三個人使用的技能，真是不好意思⋯⋯但是，我絕對會希望你能選擇。」
「不會有不選你的事。塔爾特是我的家人，也是我重要的助手。」

一開始只是作為工具。但是，現在不是那樣的。

⋯⋯為了對我有好感，使用了洗腦技術的我說這樣的話也許很厚顏無恥，但是我相信我們的羈絆是真實的。

「是！對我來說羅格大人就是一切。還有兩個人決定是誰了嗎？」
「一個決定是迪雅。」

迪雅是我的戀人，總有一天會結合的。就算除去這一點，據我所知也是最優秀的魔法使。
沒有不選擇的理由。

「還有一個人呢？」
「有點苦惱。順利的話就是瑪哈了。但是，那個孩子在後方支援，戰鬥力並不是那麼重要。如果要有即戰能力的話，也有選擇父親的方法⋯⋯暫時保留。如果覺得有必要，到時候再選吧。」

一輩子只能挑三個人，不要著急。
倒不如說，如果有更好的選擇的話。

「作為我來說，瑪哈比較好。瑪哈和我一樣喜歡羅格大人。」
「我會考慮的。我們該回去了。我被邀請參加晚宴。」
「我開始緊張了。為了不讓羅格大人丟臉，我會努力的。」

我不擔心。
因為塔爾特的女僕技能已經是一流的了。


◇

晚宴，那不是王家預定的，和我們一樣被允許在王城逗留的貴族策劃的。

參加者是允許在王城停留的高貴貴族及其傭人。
共計五十人左右。

像托瓦哈迪這樣的男爵家，儘是些容易被嚇跑的人。
我們一進去，視線就集中起來。

首先，成為話題的我，然後是媽媽，塔爾特，迪雅。
雖然都是些看膩了美女的人，但即便如此，這三人還是出類拔萃，無論如何都會吸引人的目光吧。
一方面感到自豪，另一方面也有曖昧的感情。

塔爾特慌慌張張的，但是母親和迪雅已經習慣了。
也有熟識的面孔。

諾伊斯・蓋菲斯。

作為這個國家的四大公爵家的繼承人，同班同學。
他輕輕地使了個眼色。

「感謝您這次的邀請。」

我父親行了禮，所以我們也效法了。
遵照對面的指示，入座，塔爾特在背後等候。
本來是下座，但這次是主賓，被帶到了上座。

「托瓦哈迪男爵，明明有受勲儀式的準備，卻把您叫出來，真是不好意思。我真想跟您兒子說幾句。請坐。」

蓋菲斯公爵笑眯眯地笑著，但那雙眼睛卻沒有笑。
中等身材的白髮。端正的面容，比什麼都有智慧的容光。
是真正的貴族。華麗的打扮，卻沒有一點惡意，很協調。

⋯⋯環顧四周，有很多正經的貴族。

大概是他主辦的，每個人都是他挑選出來的吧。
物以類聚。是因為正經貴族聚集了正經貴族，或者蓋菲斯公爵正在教育他的部下？

一入座，菜就端上來了。

「原來如此。」

小聲嘟囔著。
今天的料理都是使用北方的素材。

前菜三種，是在叫蘇維蝦的北方以外不能採集的山菜拌了胡桃的東西，同樣的在北方棲息的叫春田雉雞的大型山雞的鳥火腿的漿果調味汁，在北方河裡棲息的鮭魚（冰漬的生魚片）

接下來上來的肉料理也是北方的料理。在北方使用的鄉土味增燉的熊肉。
說起來，從麵包來看，使用的是耐寒的黑麥。

在王城內，只要說一句話，儘管城裡供應了飯菜，但這道菜都是將蓋菲斯領的材料帶進來，讓那位廚師來做菜。
這既不是懷念當地的料理，也不是向晚宴的參加者宣傳自己的領地之美。
在這種場合，通過擺出這樣的料理，來展示自己的立場和意志。

王族，我不服從中央的決定，蓋菲斯領按照自己的意志行動。

「今天的料理怎麼樣？比中央的料理，更合你的口味吧？」

視線的前方是我。

「是啊。好吃，但是中央的料理也不錯。」

這個問題的意圖是北方，即蓋菲斯公爵家。
所以說，現狀是不能接受那個提案的。
場地的空氣變得有點緊張，配合那個父親開口說。

「不要再這樣繞著中央風轉彎了。我保證這裡沒有被竊聽，也沒有壞人⋯⋯只要沒有告密者混進蓋菲斯公爵召集的人就沒問題了吧。」

對於父親的話，蓋菲斯公爵的周圍激揚起來。

「我們之中不可能有那樣的東西吧！」
「托瓦哈迪男爵，你是不是太得意忘形了！」
「這真是失禮了。多多少少有些顧慮。」

拐彎抹角的做法，在萬一的時候不被人說三道四。
如果，在剛才的對話中，願意站在蓋菲斯公爵這邊嗎？說這樣的話，如果公開的話，很有可能被認為是叛逆。
反過來說，如果在這裡的對話不泄漏的話，也可以不做那樣的事。

「嗯，這樣啊。我相信大家。所以直截了當地問吧。此次中央的決定。我們是不會接受的。話說回來，你能殺死魔族嗎？」
「不知道。上次的魔族殺掉了，不過，沒能殺死。無論怎麼殺都會再生，如果沒有勇者埃波納的話，我就只能逃跑了吧。」
「哦，即使殺不死，也能殺掉嗎？這令人難以置信。你為什麼能那樣做？」
「持有Ｓ等級技能。」
「那是怎樣的力量？」
「知道那個技能就等於公開我的弱點，至於是什麼技能請讓我保持沉默。」

本來，擁有Ｓ等級技能的是每一億人中只有一個。
可以說只要擁有那個就有英雄的資質。

「好吧。但是，如果殺不死就沒有意義了吧？」
「我們發明了一種方法來殺掉它。但是，還沒有得到證實。下次有魔族出現，在試用之前不能斷言可以殺。因此有殺的可能性這樣的說法。」

藏起來也沒用，老老實實地回答。

「原來如此⋯⋯如果能證實的話，那就太了不起了。是歷史上第一個被勇者以外的人殺害的魔族。」

這個人知道那是多麼大事件。
不僅是我，也許其他人也都能殺掉魔族。

「所以，與其讓我因事故或疾病而死，讓勇者留在中央的正當性消失，不如以這種可能性為賭注吧⋯⋯而且，我也不想死，所以我會全力抵抗。殺害擁有Ｓ等級技能的我是相當辛苦的。」
「哈哈哈，好像被看穿了呢。」

從大道上被監視著，進入王都後被騷亂的人接管了，注意到了這一點。
總而言之，他們想，如果我殺不了魔族，現在不把勇者送出地方這樣的決定，就可以減少被害的後果了。

「你很有意思。托瓦哈迪男爵有個好兒子。」
「恩，羅格是我的驕傲。故事不是就這樣結束的吧？」
「嗯，因為這次的事，我們對中央感到厭倦了。雖然王室還挺正經的，但被他們控制了。」

那些傢伙是四大公爵的東西兩家。

北和南，蓋菲斯公爵家和托瓦哈迪有著密切聯繫的那個公爵家是正經的，但東西是所謂惡意義上的貴族。

「如果羅格君真的能殺死魔族的話，你今後也會不斷地揚名吧。東西兩家不容忽視。只要我們吸收勇者和你的力量，就能消除他們的影響力。能否借給我力量？當然，我們會支付報酬，我會全力支持。」

能夠得到大貴族的支援是值得慶幸的。
得到蓋菲斯公爵的後盾也是在貴族社會中生存下去的力量。
但是，如果被關在這裡的話，行動變得困難也是事實，真讓人煩惱。

「就是說要拉攏我，具體要怎麼做？」

一邊思考，一邊催促對方先說。

「我家和托瓦哈迪結了婚。血脈是最緊密的。我的兒子，諾伊斯和你們美麗的千金，克勞迪雅」
「我拒絕」

即答。

這個建議不值一提。
我不會把迪雅交給你的。
我決定在第二次的人生中隨心所欲地生活。
因此，為了自己的戀愛，就踢倒大貴族的提案吧。

「不是當主，是你回答的嗎？」
「是的，你需要的不是托瓦哈迪的力量，而是作為第二勇者的力量。爸爸也會交給我的。」
「沒錯，就交給羅格吧。」

蓋菲斯公爵微微皺起眉頭，或許是因為被拒絕的原因吧。

純粹得失計算的話是不可能踢倒的提案。
他們本來就打算表示出最大限度的誠意。

並不是圍繞著迪雅訂婚對象的貴族，而是蓋菲斯公爵家，那個也選了繼承人。

「⋯⋯是嗎，雖然很遺憾，但還是期待你的大顯身手。當你打倒魔族的時候，再出現吧。接下來將提出一個不同的提案。」
「嗯，我期待著。」
「恩，麻煩的話到此結束。請從這裡開始享受純粹的料理。我們的廚師製作的甜品是絕品的。」
「就這麼辦吧。」

完全沒有挽留。
看了我的反應，大概是發現完全沒有餘地了吧。

在那之後，我純粹地享受了料理。
迪雅喜歡北方菜。看了這個，邊吃邊推測食譜，總覺得想像到了。

下次為迪雅做做看吧。還有更美味的東西。
我喜歡她。所以不讓她被奪走，也想做讓她高興的事。