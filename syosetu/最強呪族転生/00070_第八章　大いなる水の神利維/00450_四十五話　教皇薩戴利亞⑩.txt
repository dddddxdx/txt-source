「如何，亞貝爾大人！亞貝爾大人只是邊境地的一介魔術師，真是太過可惜了！還請您擔任國主，引導民眾吧！」

想讓我成為教皇的薩特利亞，表情沒有任何迷惘。但是，我不了解利維伊教，對利維伊也只有負面的感情，而且我本來也不認為自己有成為宗教或國家之主的器量。

「那、那個，我，應該不是薩特利亞所想象的那樣優秀的人吧……多半是情報在哪裡被曲解了噢。而且，教皇什麼的……這種事情，我承擔不起啊。明明我一點都不了解利維伊教……」

我完全搞不懂薩特利亞到底對我有什麼期待，但不好意思，我沒有任何理由要同意。她是有預先調查過我的事情，但肯定不是充分的調查。被利維伊限制了行動，她大概做不出顯眼的動作。

「我聽聞亞貝爾大人因為迪恩拉特王國法律對魔法的規定而煩惱，但要是您當上教皇的話，對聖典的解釋可以隨心所欲噢！而且，這個利維拉斯國有豐富的稀有魔礦石，古代留下來的未解析魔術石板也保存了不少！研究對象絕不是問題！至於人員，我會為您準備從國內遴選聚集來的、利維拉斯國最高水平的魔術師團！不會有不堪魔術訓練的人！他們為了國家的存亡，直到倒下為止都會一直向亞貝爾大人學習的！」

「真的嗎！？」

我忍不住身體前傾，大叫出來。

佩特羅表情呆然地看向薩特利亞、我、然後是利維伊。這怎麼說都太過出乎意料。

「薩特利亞……你既然已經預先調查到這種程度，還打算把國家出賣給惡魔？你腦子還正常嗎！？在重壓下崩潰變成笨蛋了嗎！？」

佩特羅非難似的指向薩特利亞。

『薩特利亞……你這傢伙！是打算要引入與余敵對的亞貝爾來牽制余嗎！你居然把余小看到如此地步！一派胡言！你這麼心急要把教皇之位讓出去，還居然是這個迪恩拉特王國的、捕獲了其他三大神官的男人？民眾是不會接受的』

利維伊張大嘴巴叫喊道。

「……我只是個花瓶，利維拉斯國的民眾或多或少已經覺察到了。既然是利維伊大人所指名的，表面上才沒有人反對吧。而且，亞貝爾大人不是持有了利維伊大人的槍嗎」

薩特利亞眼神冰冷地看著利維伊說道。

『你要余、余公開宣告將亞貝爾任命到國家中樞嗎！這種猶如將余絞首的事情，余怎麼可能會做！』

「做不到的話，那利維伊大人就唯有在這裡把亞貝爾大人消滅，善待我和國家。利維伊大人繼續固執不肯接受的話，我是不會使用龍脈的！」

『薩、薩特利亞，你這傢伙……讓亞貝爾擔任國家主權，把余、余當成區區擺設嗎？你……真的覺得做這種事情，會被允許……？』

利維伊像是要說服，或者是懇求一樣對薩特利亞說道。

「……沒錯，就是這樣。非常抱歉利維伊大人，不這樣做的話，利維拉斯國會滅亡的」

薩特利亞平靜地向利維伊低下頭。

「要獲得迪恩拉特王國的信任，將國家中樞交付給佩特羅大人看重的魔術師是最好的辦法。再加上，不把利維伊大人的影響力減弱的話，對方是不會接受吧。而且，在四大神官失去三人的現在，為了抑制各地的內亂，我們需要本拉多閣下之上的鍊金術師。利維伊大人，這是由於您的肆意妄為而導致的後果。亞貝爾大人的話，一人就可以完全解決」

利維伊雙眼充血，嘴巴微動，像是要說什麼，但沒能形成語言。之後，它的嘴裡漏出嗚咽聲。

『余……作為神的余，竟然要聽從這個小女孩的妄言，以擺設的姿態度過餘生麼……？』

它說著說著，穨然地閉上三只眼睛。佩特羅沒有像之前那樣嘲弄薩特利亞，神態嚴肅地看著她。

「……如、如此程度的覺悟，真的有呢」

終、終於，利維伊和佩特羅被說服了……？也就是說，欸，我、我要成為教皇？

我悄悄看向利維伊。它眼神非常痛苦地看向我，又別開臉。它現在的模樣與最初蓄勢待發的樣子迥然不同。我的討伐任務似乎完全讓龍脈完成了，關鍵的龍脈被薩特利亞掌握真是太好了。其他的大神官肯定會大吃一驚的。

「欸……亞貝爾真的要成為利維拉斯國的教皇嗎？」

梅婭戰戰兢兢地問我。我側面看向薩特利亞，她對我露出非常美麗的笑容。

「怎麼說，好像是的……」

我不得不填補四大神官的空缺麼。我和薩特利亞是固定人選，之後是雅爾塔米亞和本爺嗎？收集家……他不行呢。大概叫他也不會來吧。不，現在還不能確定。利維拉斯國裡還藏著厲害的人才也說不定。不如乾脆擴增到七個人左右吧？

「……總覺得，亞貝爾變得好遙遠呢」

梅婭有點寂寞地說著。我抱起手稍微思考。

「梅婭想當利維伊的大神官嗎？」

「可可以嗎！？」

梅婭叫了起來，看向薩特利亞。薩特利亞笑著點頭。

「嗯，可以，不是別人，只要是亞貝爾大人推薦的話，就沒有問題。只要亞貝爾大人能夠接受，多多少少的任性我也是會同意的。利維拉斯國有著很多問題，不過，讓我們一起努力，把這裡變成和平安定的國家吧」

「和平安定的國家……啊！」

梅婭突然想起來什麼似的叫了出來。

「有什麼問題嗎，梅婭大人？」

薩特利亞微笑著詢問梅婭。

「啊，不是，不好意思，沒什麼問題。只是想到了一個疑問……因為有點失禮你也許會不喜歡……」

梅婭像是羞於說出來似的把手放在嘴上，向薩特利亞表示歉意。

「不會不會，若是有疑問的話，我都可以回答的。宗教不同，思維也會不同，我對此很尊重的，請不必擔心」

「額，那，我就說了……梅婭想到的是，額，假如，即使沒有了水神大人，一切都會順當的吧？看啦，這樣的話佩特羅大人也會妥協的呢！」

薩特利亞表情痙攣地僵住了。

利維伊那邊傳來了巨大的聲響。巨大的藍色手臂把宮殿的一根柱子握碎了。它的焦慮似乎超出了容忍的限度。

大概，我的表情也稍微僵住了。

「梅婭……這是做不到的，薩特利亞小姐之所以能以一派將利維伊教的各個教派統一到如今的程度，最大的原因就是原教神的利維伊成為了己方。除去了利維伊的瞬間，就等同於宣告薩特利亞小姐所做的統一沒有任何的正當性」

這樣做的話，恐怕內亂會超過現在的規模，變成全民皆敵也不奇怪。佩特羅預測利維伊被殺之後國內會陷入大混亂，而利維伊要是被薩特利亞這邊公然處決的話，混亂會更加一發不可收拾。想要與利維拉斯國和解，不得不將利維伊作為擺設繼續留在國內。

「是、是這樣呢，梅婭不說了，對不起……」

「不會不會，請不要在意！」

薩特利亞很僵硬地笑著回應，戰戰兢兢地確認著利維伊的樣子。

無論如何，無事發生地順利解決了。過幾天佩特羅跟王家商量，應該可以締結正式的同盟吧。不過，我接下來該做什麼呢……。

「佩特羅先生，這樣就沒問題了吧？」

我看向佩特羅。佩特羅似乎在思考著什麼事情，他眉頭皺起，咬著下唇。

「……哈啊，所以我才說別做談話什麼的呢，回想起來真是討厭」

他發出嘆息，面向我說道。

「亞貝爾醬，準備戰鬥吧」

「欸……佩、佩特羅先生，你不是已經認可……」

佩特羅輕輕地搖頭。

「已經完蛋了呢，這個國家。去當一個行將毀滅的國家的君主什麼的，為什麼要附和這種夢話？你只是被牽著鼻子走而已，根本沒有相應的覺悟呢。現在，我就稍微認真地說吧。薩特利亞，你應該也明白的吧？我會盡可能以最仁慈的形式俘虜你的，請放棄掙扎吧」
