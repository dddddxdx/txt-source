# CONTENTS

給鍬形蟲一記手刀結果穿越時空了  
クワガタにチョップしたらタイムスリップした  
给锹形虫一记手刀结果穿越时空了  

作者： タカハシ ヨウ  



- :closed_book: [README.md](README.md) - 簡介與其他資料
- [格式與譯名整合樣式](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E7%B5%A6%E9%8D%AC%E5%BD%A2%E8%9F%B2%E4%B8%80%E8%A8%98%E6%89%8B%E5%88%80%E7%B5%90%E6%9E%9C%E7%A9%BF%E8%B6%8A%E6%99%82%E7%A9%BA%E4%BA%86.ts) - 如果連結錯誤 請點[這裡](https://github.com/bluelovers/node-novel/blob/master/lib/locales/)
-  :heart: [EPUB](https://gitlab.com/demonovel/epub-txt/blob/master/girl/%E7%B5%A6%E9%8D%AC%E5%BD%A2%E8%9F%B2%E4%B8%80%E8%A8%98%E6%89%8B%E5%88%80%E7%B5%90%E6%9E%9C%E7%A9%BF%E8%B6%8A%E6%99%82%E7%A9%BA%E4%BA%86.epub) :heart:  ／ [TXT](https://gitlab.com/demonovel/epub-txt/blob/master/girl/out/%E7%B5%A6%E9%8D%AC%E5%BD%A2%E8%9F%B2%E4%B8%80%E8%A8%98%E6%89%8B%E5%88%80%E7%B5%90%E6%9E%9C%E7%A9%BF%E8%B6%8A%E6%99%82%E7%A9%BA%E4%BA%86.out.txt) - 如果連結錯誤 請點[這裡](https://gitlab.com/demonovel/epub-txt/blob/master/girl/)
- :mega: [https://discord.gg/MnXkpmX](https://discord.gg/MnXkpmX) - 報錯交流群，如果已經加入請點[這裡](https://discordapp.com/channels/467794087769014273/467794088285175809) 或 [Discord](https://discordapp.com/channels/@me)


![導航目錄](https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=https://gitlab.com/novel-group/txt-source/blob/master/girl/給鍬形蟲一記手刀結果穿越時空了/導航目錄.md "導航目錄")




## [第一卷](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7)

- [１　躲避球會打破人與人之間的信賴](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/%EF%BC%91%E3%80%80%E8%BA%B2%E9%81%BF%E7%90%83%E6%9C%83%E6%89%93%E7%A0%B4%E4%BA%BA%E8%88%87%E4%BA%BA%E4%B9%8B%E9%96%93%E7%9A%84%E4%BF%A1%E8%B3%B4.txt)
- [２　非常感謝如此溫柔親切的粘人](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/%EF%BC%92%E3%80%80%E9%9D%9E%E5%B8%B8%E6%84%9F%E8%AC%9D%E5%A6%82%E6%AD%A4%E6%BA%AB%E6%9F%94%E8%A6%AA%E5%88%87%E7%9A%84%E7%B2%98%E4%BA%BA.txt)
- [３　避免全滅，人類避免了全滅。](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/%EF%BC%93%E3%80%80%E9%81%BF%E5%85%8D%E5%85%A8%E6%BB%85%EF%BC%8C%E4%BA%BA%E9%A1%9E%E9%81%BF%E5%85%8D%E4%BA%86%E5%85%A8%E6%BB%85%E3%80%82.txt)
- [４　那無機質的天空下也能看見的希望](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/%EF%BC%94%E3%80%80%E9%82%A3%E7%84%A1%E6%A9%9F%E8%B3%AA%E7%9A%84%E5%A4%A9%E7%A9%BA%E4%B8%8B%E4%B9%9F%E8%83%BD%E7%9C%8B%E8%A6%8B%E7%9A%84%E5%B8%8C%E6%9C%9B.txt)
- [５　給鍬形蟲一記手刀結果穿越時空了](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/%EF%BC%95%E3%80%80%E7%B5%A6%E9%8D%AC%E5%BD%A2%E8%9F%B2%E4%B8%80%E8%A8%98%E6%89%8B%E5%88%80%E7%B5%90%E6%9E%9C%E7%A9%BF%E8%B6%8A%E6%99%82%E7%A9%BA%E4%BA%86.txt)

